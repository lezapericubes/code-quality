<?php

namespace Cesi\Msi\Lezap\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FileReadCommand extends Command
{
    protected static $defaultName = 'file:read';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('--file', '-f',InputArgument::OPTIONAL, 'file')
            ->addOption('--delimiter', '-d',InputArgument::OPTIONAL, 'delimiter')
            ->addArgument('versions', InputArgument::OPTIONAL, 'delimiter')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $arguments = [null];
        if($input->getArgument('versions')){
            $output->writeln("Cupcake");
        }
        $optionFile = $input->getOption('file');
        $optionDelimiter = $input->getOption('delimiter');

        if(isset($optionFile) && isset($optionDelimiter)){
            $myFile = fopen($optionFile, "r") or die("Unable to open file!");
            
            while (($data = fgetcsv($myFile, 1000, $optionDelimiter)) !== FALSE) {
                if (strpos(strtolower(strval($data[0])), "http") ===false) {
                    // Continue because maybe only one of the lines had bad data
                    $output->writeln("");
                    break;
                }
        
                $headers = get_headers($data[0], 1);
        
                $cacheControl = isset($headers["Cache-Control"]) ? $headers["Cache-Control"] : null;
                $contentType = isset($headers["Content-Type"]) ? $headers["Content-Type"] : null;
                $expires = isset($headers["Expires"]) ? $headers["Expires"] : null;
        
                // We check if the headers are in an array or not, if they are we implode them into string separated by commas
                if(is_array($cacheControl)){
                    $cacheControl = implode(",", $cacheControl);
                }
                if(is_array($contentType)){
                    $contentType = implode(",", $contentType);
                }
                if(is_array($expires)){
                    $expires = implode(",", $expires);
                }
        
                // If we only found one
                if(count($data) == 1){
                    $output->writeln("Bad delimiter");
                }
        
                $dataArray = array($data[0], $data[1], $cacheControl, $contentType, $expires);
        
                // Print the data in a CSV format
                // Creating a temporary file to have the right format for CSV
                $csv = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
                fputcsv($csv, $dataArray, ";");
                rewind($csv);
                $output->writeln(stream_get_contents($csv));
            }
            
            fclose($myFile);
        }
        return Command::SUCCESS;
    }

    
    function printCSV(array $dataArray){
        // Creating a temporary file to have the right format for CSV
        $csv = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
        fputcsv($csv, $dataArray, ";");
        rewind($csv);
        print_r(stream_get_contents($csv));
        return (stream_get_contents($csv));
    }
}
