# Code Quality
[![Latest Stable Version](https://img.shields.io/packagist/v/lezapericubes/code-quality.svg?style=flat-square)](https://packagist.org/packages/lezapericubes/code-quality)
[![License: MIT](https://img.shields.io/badge/license-MIT-green)](./LICENSE)

## Install the module
To get the headers of your file you can follow those instructions:

Clone the repository by typing the following command:

```
git clone https://gitlab.com/lezapericubes/code-quality.git
```

Or with **composer** :
```
composer require lezapericubes/code-quality
```

If you already have docker installed, you can go into the created directory then type those commands: 

```
docker volume create local-composer-repository
docker-compose up -d
docker-compose exec app composer install -vvv
```

Once you ran this command, go into the folder it was cloned into then paste your .CSV file 

You can then run the following command to get those headers:

```
docker-compose exec app php src/Start.php [FILENAME] [DELIMITER]
php bin/console file:read -f file.csv -d ";"
```

To view the version:
```
docker-compose exec app php src/Start.php [FILENAME] --version
php bin/console file:read versions
```

## Support

- If you have any issue with this code, feel free to [open an issue](https://gitlab.com/lezapericubes/code-quality/-/issues/new).


## Contact

For further information, contact us:

- by email: florianleking@fabulous.com

## Authors

- **Bruno Fache** - *Maintainer* - [It's me!](https://gitlab.com/Luxoris)
- **Rémi ARLEN** - *Maintainer* - [It's me!](https://gitlab.com/rarlen)
- **Killian MATTER** - *Maintainer* - [It's me!](https://gitlab.com/killou)
- **Florian FORNAZARIC** - *Maintainer* - [It's me!](https://gitlab.com/f.fornazaric)


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

***That's all folks!***
