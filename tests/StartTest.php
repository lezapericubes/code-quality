<?php

namespace Cesi\Msi\Lezap\Tests;

use function Cesi\Msi\Lezap\start;
use function Cesi\Msi\Lezap\read_csv_file_get_headers;
use Cesi\Msi\Lezap\Command\FileReadCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;


final class StartTest extends TestCase
{

    public function testNotGoodArgumentStart() : void
    {
        $argc = 4;
        $argv = [];
        $this->assertSame("BAD FORMAT, USAGE: start.php [FILENAME] (DELIMITER)", start($argc,$argv), "No goods arguments test failed");
    }

    public function testNotGoodSeparatorReadCsv() : void
    {
        $file = fopen("file.csv", "r");
        $this->assertSame("Bad delimiter", read_csv_file_get_headers($file, ","), "No goods arguments test failed");
    }

    public function testGoodSeparatorStart() : void
    {
        $argc = 3;
        $argv = ['Start.php','file.csv',';'];
        $this->assertSame("Done", start($argc,$argv), "No goods arguments test failed");
    }

    public function testVersion() : void
    {
        $argc = 2;
        $argv = ['Start.php','--version'];
        $this->assertSame("Cupcake", start($argc, $argv, "Version test failed"));
    }
    public function testBadArgument() : void
    {
        $argc = 2;
        $argv = ['Start.php','LOL'];
        $this->assertSame("BAD USAGE: start.php --version", start($argc, $argv, "BAD ARGUMENT test failed"));
    }
}