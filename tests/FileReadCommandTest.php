<?php


namespace Cesi\Msi\Lezap\Tests;

use function Cesi\Msi\Lezap\start;
use function Cesi\Msi\Lezap\read_csv_file_get_headers;
use Cesi\Msi\Lezap\Command\FileReadCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

final class FileReadCommandTest extends TestCase
{
    public function testCommandVersionComposer() : void
    {
        $command = new FileReadCommand();
        $commandTester = new Commandtester($command);

        $commandTester->execute([
            'versions' => 'versions'
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString(sprintf('Cupcake'), $output);
    }

    public function testCommandFileComposer() : void
    {
        $command = new FileReadCommand();
        $commandTester = new Commandtester($command);

        $commandTester->execute([
            '--file' => 'file.csv',
            '--delimiter' => ';'
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString(sprintf('Mondial de handball'), $output);

    }
    

}
