<?php

namespace Cesi\Msi\Lezap;


/**
 * Start the programm and execute it
 * @param int|null $argc
 * @param array|null $argv
 * @return string
 */
function start(?int $argc, ?array $argv): string
{
    // We test if there's the right number of arguments
    if($argc == 2){
        if($argv[1] == "--version"){
            return "Cupcake";
        }else{
            return "BAD USAGE: start.php --version";
        }
    }
    else if($argc != 2 && $argc != 3){
        return "BAD FORMAT, USAGE: start.php [FILENAME] (DELIMITER)";
    }else{
        $separator = ";";
        if($argc == 3){
            $separator = $argv[2];
        }

        // $argv represents every arguments, $argv[0] is the script name
        $myFile = fopen($argv[1], "r") or die("Unable to open file!");
        // Output one line until end-of-file
        read_csv_file_get_headers($myFile, $separator);
        fclose($myFile);
    }
    return ("Done");
}

function read_csv_file_get_headers($file, $separator) {
    while (($data = fgetcsv($file, 1000, $separator)) !== FALSE) {
        if (strpos(strtolower(strval($data[0])), "http") ===false) {
            // Continue because maybe only one of the lines had bad data
            return "";
        }

        $headers = get_headers($data[0], 1);

        $cacheControl = isset($headers["Cache-Control"]) ? $headers["Cache-Control"] : null;
        $contentType = isset($headers["Content-Type"]) ? $headers["Content-Type"] : null;
        $expires = isset($headers["Expires"]) ? $headers["Expires"] : null;

        // We check if the headers are in an array or not, if they are we implode them into string separated by commas
        if(is_array($cacheControl)){
            $cacheControl = implode(",", $cacheControl);
        }
        if(is_array($contentType)){
            $contentType = implode(",", $contentType);
        }
        if(is_array($expires)){
            $expires = implode(",", $expires);
        }

        // If we only found one
        if(count($data) == 1){
            return("Bad delimiter");
        }

        $dataArray = array($data[0], $data[1], $cacheControl, $contentType, $expires);

        // Print the data in a CSV format
        printCSV($dataArray);
    }
}

function printCSV(array $dataArray){
    // Creating a temporary file to have the right format for CSV
    $csv = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
    fputcsv($csv, $dataArray, ";");
    rewind($csv);
    print_r(stream_get_contents($csv));
    return (stream_get_contents($csv));
}


print(start($argc, $argv)."\n");